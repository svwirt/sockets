#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <dirent.h>

char* getFile(char* file_name);
void sendMessage(int sock, char* buffer);
void sendNum(int sock, int numb);
void getMessage(int sock, char* buffer, size_t size);
int getNum(int sock);
int startServer(int port_number);


int main(int argc, char *argv[]) {

    int sockfd, newsocketfd, datasocket, server_port_number, pid;

    // must be 2 arguments - server and port num
    if (argc < 2) {
        printf("Error! invalid number of arguments");
        printf("Please reference README\n");
        exit(1);
    }

    char *p;
    long arg = strtol(argv[1],&p,10);
    // https://stackoverflow.com/questions/9748393/how-can-i-get-argv-as-int/38669018
    // checks that port num is not out of range
    if (arg < 1024 || arg > 65535) {
        printf("Error! invalid port number");
        printf("Please reference README\n");
        exit(1);
    }

    // convert the server port argument to a number
    server_port_number = atoi(argv[1]);
    // https://books.google.com/books?id=o7I3XjElmSYC&pg=PA71&lpg=PA71&dq=server_port_number+%3D+atoi(argv%5B1%5D);&source=bl&ots=-W7G0M_OHd&sig=khvjv8dDarhrSqvLVl-Rm1qYI0o&hl=en&sa=X&ved=2ahUKEwionJzS1OjcAhUm24MKHZWaCJ8Q6AEwA3oECAcQAQ#v=onepage&q=server_port_number%20%3D%20atoi(argv%5B1%5D)%3B&f=false
    // start up a server
    sockfd = startServer(server_port_number);
    // http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
    printf("Server open on %d\n", server_port_number);

    // loops until SIGINT (control C)
    while(1) {
        
        // accept connection and fork
        newsocketfd = accept(sockfd, NULL, NULL);
        pid = fork();
        // http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html

        // if it forked 
        if (pid == 0) {
            close(sockfd);
            int command = 0;
            int dPortNum;
            int newsocket;
            char comString[3] = "\0";

            printf("Connection from flip2.\n");
            // receives the message from the client
            getMessage(newsocketfd, comString, 3);
            dPortNum = getNum(newsocketfd);
            

            // checks which command was sent
            if (strcmp(comString, "-l") == 0) {
                char* path[100];
                int i = 0;
                int dirSize = 0;
                printf("Directory list on port %d.\n", dPortNum);
                // directory struct
                DIR *d;
                struct dirent *dir;
                int size = 0;
                int fileCount = 0;

                // open the current directry
                d = opendir(".");
                if (d) {
                    int i = 0;
                    // adds the contents to path
                    // logs size of content
                    // https://stackoverflow.com/questions/44164436/check-if-item-on-directory-is-folder-in-c
                    while ((dir = readdir(d)) != NULL) 
                    {
                        if (dir->d_type == DT_REG) 
                        {
                            path[i] = dir->d_name;
                            size += strlen(path[i]);
                            i++;
                        }
                    }
                    fileCount = i - 1;
                }
                closedir(d);
                 // overall size and the number of files
                dirSize = size + fileCount;
                // start server
                newsocket = startServer(dPortNum);
                // accept a connection
                datasocket = accept(newsocket, NULL, NULL);
                // send the dirSize of the directory contents to the client
                sendNum(datasocket, dirSize);

                printf("Directory going to flip2: %d\n", dPortNum);

                // send the contents of the directory
                while (path[i] != NULL) 
                {
                    sendMessage(datasocket, path[i]);
                    i++;
                }
                // close all sockets
                close(newsocket);
                close(datasocket);
                exit(0);
            
            }
            // if the request uses a -g
            if (strcmp(comString, "-g") == 0) 
            {
                int i = getNum(newsocketfd);
                char file_name[255] = "\0";

                // gets the file name
                getMessage(newsocketfd, file_name, i);
                printf("File \"%s\" requested on port %d.\n", file_name, dPortNum);

                // checks if the file can be found
                // https://stackoverflow.com/questions/13508712/does-access-function-check-the-existence-of-file
                if (access(file_name, F_OK) == -1) 
                {
                    printf("No such file. Sending error message to flip2: %d\n", server_port_number);
                    char error_message[] = "FILE NOT FOUND";

                    // sends the error message length and message
                    sendNum(newsocketfd, strlen(error_message));
                    sendMessage(newsocketfd, error_message);

                    close(newsocket);
                    close(datasocket);
                    exit(1);
                }

                else 
                {
                    // the file was found
                    char message[] = "FILE FOUND";

                    // send back the length and message
                    sendNum(newsocketfd, strlen(message));
                    sendMessage(newsocketfd, message);
                }

                printf("Sending file \"%s\" to flip2: %d\n", file_name, dPortNum);

                // start server
                newsocket = startServer(dPortNum);
                datasocket = accept(newsocket, NULL, NULL);
                // https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpxbd00/accept.htm

                // send the file 
                char* transFile;
                transFile = getFile(file_name);

                // sends the length and message
                sendNum(datasocket, strlen(transFile));
                sendMessage(datasocket, transFile);

                close(newsocket);
                close(datasocket);
                exit(0);
            }
            // if the argument was not -l or -g
            else
            {
                perror("Error! incorrect argument.  Please see README");
            }
            exit(0);
        }

    }
}


// gets the contents of the passed in file name
char* getFile(char* file_name) {
    char *transFile = NULL;

    // opens the file
    FILE* file = fopen(file_name, "r");

    // if does not exist
    if (file == NULL) {
        perror("ERROR: No such file");
        exit(1);

    }
    // if the the file was opened
    else 
    {
        
        // start at the beginning of the file
        // https://www.tutorialspoint.com/c_standard_library/c_function_fseek.htm
        if (fseek(file, 0L, SEEK_END) == 0) 
        {
            long buffer_size = ftell(file);

            // http://pubs.opengroup.org/onlinepubs/009695399/functions/ftell.html
            transFile = malloc(sizeof(char) * (buffer_size + 1));

            // confirms can start at beginning
            if (fseek(file, 0L, SEEK_SET) != 0) 
            {
                perror("cant read file");
                exit(1);
            }
            // reads contents into fs
            // https://www.ibm.com/support/knowledgecenter/en/ssw_ibm_i_72/rtref/fread.htm    
            size_t new_length = fread(transFile, sizeof(char), buffer_size, file);
            transFile[new_length++] = '\0';
        }
    }
    fclose(file);
    return transFile;
}


// sends a message to the client
void sendMessage(int sock, char* buffer) {
    ssize_t i;
    size_t size = strlen(buffer) + 1;
    size_t message = 0;

    // sends data until all is sent
    while (message < size) {
        i = write(sock, buffer, size - message);
        // https://www.ibm.com/support/knowledgecenter/en/SSB23S_1.1.0.15/gtpc2/cpp_write.html
        message += i;

        // increases the message amount sent
        if (i == 0) {
            message = size - message;
        }
    }
}
// sends a number to the client
void sendNum(int sock, int num) {
    ssize_t i = 0;
    // sends a number 
    i = write(sock, &num, sizeof(int));
    // https://www.ibm.com/support/knowledgecenter/en/SSB23S_1.1.0.15/gtpc2/cpp_write.html

}

// receives a message from the client
void getMessage(int sock, char* buffer, size_t size) {
    char buff[size + 1];
    ssize_t i;
    size_t message = 0;

    // reads through entire message
    while (message < size) {
        i = read(sock, buff + message, size - message);
        message += i;

    }
    strncpy(buffer, buff, size);
}

// recieves a number from the client
int getNum(int sock) {
    int num;
    ssize_t i = 0;
    // reads the number from the client
    i = read(sock, &num, sizeof(int));
    // https://www.ibm.com/support/knowledgecenter/en/SSLTBW_2.3.0/com.ibm.zos.v2r3.bpxbd00/rtrea.htm
    return num;
}

// starts up a server
int startServer(int port_number) {

    // http://www.cs.rpi.edu/~moorthy/Courses/os98/Pgms/socket.html
    int sockfd = socket(AF_INET, SOCK_STREAM, 0);

    // server info
    //https://www.freebsd.org/doc/en/books/developers-handbook/sockets-essential-functions.html
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(port_number);
    server.sin_addr.s_addr = INADDR_ANY;
    int optval = 1;
    setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);
    
    // bind and set server to listen
    bind(sockfd, (struct sockaddr *) &server, sizeof(server));
    listen(sockfd, 10);

    return sockfd;
}
// Other references were Brewster's socket programming server.c example and Beejs guide
