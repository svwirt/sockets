# Stephanie Ayala
# 08/12/18
# Project 2
# Client for file transfer

#!/bin/python
import socket
import sys
import os
from os import path
from struct import *
from time import sleep

global name
global sNum
global command
global transFile
global dPortNum


def whichCommand():
    # if -l then get the directory
    if command == "-l":
        sleep(1)
        # contact server
        sock = contactServer(name, dPortNum)
        print("Directory from {}: {}".format(sys.argv[1], dPortNum))
        # https://stackoverflow.com/questions/4117530/sys-argv1-meaning-in-script
        # get the directory contents
        ds = sock.recv(4)
        ds = unpack("I", ds)
        # https://stackoverflow.com/questions/3753589/packing-and-unpacking-variable-length-array-string-using-the-struct-module-in-py/36793125
        rec = str(sock.recv(ds[0]), encoding="UTF-8").split("\x00")
        for item in rec:
            print(item)
        sock.close()
    # if -g then get the file
    elif command == "-g":
        # send the length of the file name
        num = pack('i', len(transFile))
        server.send(num)
        # send the file name and get message
        message = bytes(transFile + "\0", encoding="UTF-8")
        server.sendall(message)
        fileMessage = getMessage(server)

        if fileMessage == "FILE NOT FOUND":
            print("{}: {} says {}".format(sys.argv[1], sNum, fileMessage))
        elif fileMessage == "FILE FOUND":
            print("Receiving \"{}\" from {}: {}".format(transFile, sys.argv[1], dPortNum))
            sleep(1)
            # get server and file
            sock = contactServer(sys.argv[1], dPortNum)
            getFile(sock, transFile)
            print("File transfer complete.")
            sock.close()

# takes file from server
def getFile(sock, file):
    buf = getMessage(sock)
    with open(file, 'w') as f:
        f.write(buf)

# server request
def serverRequest(sock, cmd, pn):
    # send message and get number
    message = bytes(cmd + "\0", encoding="UTF-8")
    sock.sendall(message)
    # https://docs.python.org/2/library/socket.html
    num = pack('i', pn)
    sock.send(num)

# get message from server
def getMessage(sock):
    mes = sock.recv(4)
    mes = unpack("I", mes)
    # https://docs.python.org/2/library/struct.html
    rec = ""
    while len(rec) < mes[0]:
        packet = str(sock.recv(mes[0] - len(rec)), encoding="UTF-8")
        if not packet:
            return None
        rec += packet
    return rec

# contact server
def contactServer(host_name, port_number):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host_name, port_number))
    return sock

# main 
if __name__ == '__main__':
    
    # checks the number of arguments
    if len(sys.argv) not in (5,6):
        print("ERROR: wrong number of arguments")
        print("Please review README")
        exit(1)
    # checks the server host name
    elif sys.argv[1] != "flip1" or sys.argv[3] not in ("-l","-g"):
        print("ERROR: incorrect format")
        print("Please review README")
        exit(1)
    # checks if the port number is in range
    elif int(sys.argv[2]) not in range(1024,65536) or int(sys.argv[-1]) not in range(1024,65536):
        print("ERROR: port number not in range")
        print("Please review README")
        exit(1)
   
    elif sys.argv[2] == sys.argv[-1]:
        print("ERROR: data port number cannot match server port number")
        print("Please review README")
        exit(1)

    # assigns the arguments to their variables
    name = sys.argv[1] + ".engr.oregonstate.edu"
    sNum = int(sys.argv[2])
    dPortNum = int(sys.argv[-1])
    command = sys.argv[3]
    if command == "-g":
        transFile = sys.argv[4]
    else:
        transFile = None

    # contact server
    server = contactServer(name, sNum)
    # sends a request to the server
    serverRequest(server, command, dPortNum)
    # if its -l or -g do different things
    whichCommand()        
    server.close()

    # other sources
    # https://www.geeksforgeeks.org/socket-programming-python/
    #http://www.bogotobogo.com/python/python_network_programming_server_client_file_transfer.php
    # https://docs.python.org/3.4/howto/sockets.html
