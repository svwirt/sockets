// Stephanie Ayala
// Program 1 - client.c
// 07/26/2018
// This program refernces Brewster's client.c given as a basis in CS344

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <assert.h>

void message(int socketFD, char * username, char * servername);

int main(int argc, char *argv[]){                
    int socketFD, portNumber;
    struct sockaddr_in serverAddress;
    struct hostent* serverHostInfo;
    char userName[10]; 
    char serverName[10];

    if (argc < 3) { fprintf(stderr,"USAGE: %s hostname port\n", argv[0]); exit(0); } // Check usage & args

    printf("Enter a username (up to 10 characters): ");
    scanf("%s", userName);  

    // Set up the server address struct
    memset((char*)&serverAddress, '\0', sizeof(serverAddress)); 
    // Clear out the address struct
    portNumber = atoi(argv[2]); 
    // Get the port number, convert to an integer from a string
    serverAddress.sin_family = AF_INET; 
    // Create a network-capable socket
    serverAddress.sin_port = htons(portNumber); 
    // Store the port number
    serverHostInfo = gethostbyname(argv[1]); 
    // Convert the machine name into a special form of address
    if (serverHostInfo == NULL) { fprintf(stderr, "CLIENT: ERROR, no such host\n"); exit(0); }
    // Copy in the address
    memcpy((char*)&serverAddress.sin_addr.s_addr, (char*)serverHostInfo->h_addr, serverHostInfo->h_length); 
    // Set up the socket
    socketFD = socket(AF_INET, SOCK_STREAM, 0); 
    // Create the socket
    if (socketFD < 0) error("CLIENT: ERROR opening socket");
    
    // Connect to server
    if (connect(socketFD, (struct sockaddr*)&serverAddress, sizeof(serverAddress)) < 0) // Connect socket to address
        error("CLIENT: ERROR connecting");

    send(socketFD, userName, strlen(userName), 0);  
    recv(socketFD, serverName, 10, 0);

    message(socketFD, userName, serverName);   


    
    return 0;
}


void message(int socketFD, char * username, char * servername){  
    // setup buffers
    int charsRead;
    char buffer[500];                  
    char charsWritten[500];
  
    // get user input
    fgets(buffer, sizeof(buffer), stdin);  
    
    while(1){       
        printf("%s> ", username);   
        // get user input
        memset(buffer, 0 ,sizeof(buffer));
        memset(charsWritten, 0, sizeof(charsWritten));
        fgets(buffer, sizeof(buffer) - 1, stdin);
        // if user types \quit end connection
        if (strcmp(buffer, "\\quit\n") == 0){    
            break;
        }
        // make sure data is sent and recieved
        int testing = send(socketFD, buffer, strlen(buffer), 0);  
        charsRead = recv(socketFD, charsWritten, sizeof(buffer), 0);     
        if (testing < 0) error("CLIENT: ERROR writing to socket");
        if (testing < strlen(buffer)) printf("CLIENT: WARNING: Not all data written to socket!\n");
        if (charsRead == 0) error("CLIENT: ERROR reading from socket");
                                                         
        printf("%s> %s\n", servername, charsWritten);
        
    }
    // close connection
    close(socketFD);                                  

}