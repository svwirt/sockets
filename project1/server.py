#!/bin/python

# Stephanie Ayala
# Project 1 - server
# 07/27/2018

import sys
from socket import *
   
# user must enter a port number    
if len(sys.argv) != 2:             
    print "Please enter a port number"
    exit(1)
   
portNumber = sys.argv[1]
# https://docs.oracle.com/cd/E19620-01/805-4041/6j3r8iu2l/index.html
newSocket = socket(AF_INET, SOCK_STREAM)         
newSocket.bind(('', int(portNumber)))           
newSocket.listen(1)         
# initialize userName  
userName = ""
while len(userName) > 10 or len(userName) == 0:
    # get username    
    userName = raw_input("Enter a username (up to 10 characters): ")
    print "Ready to chat."
while 1:
    # establish connection
    connection, address = newSocket.accept()                         
    print "Connected".format(address)   
    clientName = connection.recv(1024)    
    connection.send(userName)
    # until client types quit
    while 1:   
        # recieve message            
        receive = connection.recv(501)[0:-1]        
        # https://docs.python.org/3/tutorial/inputoutput.html    
        print "{}> {}".format(clientName, receive)     
        # Initialize sendMessage
        sendMessage = ""
        # message cannot be longer than 500 characters
        while len(sendMessage) > 500 or len(sendMessage) == 0:
            sendMessage = raw_input("{}> ".format(userName))
        # type \quit to quit        
        if sendMessage == "\quit":
            print "......"              
            break
        connection.send(sendMessage)
    # close connection  
    connection.close()
        

        
# references
# https://docs.oracle.com/cd/E19620-01/805-4041/6j3r8iu2l/index.html
# https://docs.python.org/3/tutorial/inputoutput.html  
# https://docs.python.org/release/2.6.5/library/internet.html